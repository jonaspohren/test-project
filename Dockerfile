FROM node:12-alpine3.10
WORKDIR /var/app
COPY . .
RUN apk add yarn && \
    yarn install
EXPOSE 3000
CMD ["node", "index_1.js"]